#!/usr/bin/env bash

server_name='pihole-dns'
[ -n "$1" ] && server_name=$1

sudo podman generate systemd --new --files --name $server_name
sudo mv -Z container-$server_name.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable --now container-$server_name.service

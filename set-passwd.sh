#!/usr/bin/env bash

server_name='pihole-dns'
[ -n "$1" ] && server_name=$1

sudo podman exec -it $server_name pihole -a -p

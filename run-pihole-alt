#!/usr/bin/env bash

HOST_IP=$(ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1 -d'/')
CON_NAME="pihole-dns-alt"
CON_IP="10.0.0.252"
CON_NET="macvlan"
CON_VOL_PIHOLE="pihole"
CON_VOL_DNSMASQ="dnsmasq"

sudo podman run -d \
  --name $CON_NAME \
  --network=$CON_NET \
  --ip=$CON_IP \
  -v $CON_VOL_PIHOLE:/etc/pihole \
  -v $CON_VOL_DNSMASQ:/etc/dnsmasq.d \
  --dns=127.0.0.1 \
  --dns=1.1.1.1 \
  --restart=unless-stopped \
  --hostname $CON_NAME \
  -e VIRTUAL_HOST="$CON_NAME" \
  -e TZ="America/New_York" \
  -e WEBPASSWORD="password" \
  -e FTLCONF_REPLY_ADDR4="$HOST_IP" \
  -e WEBTHEME="default-dark" \
  -e PIHOLE_DNS_="1.1.1.1;9.9.9.9" \
  -e DNSSEC="true" \
  -e DNS_BOGUS_PRIV="true" \
  -e DNS_FQDN_REQUIRED="true" \
  docker.io/pihole/pihole:latest

printf "Starting up $CON_NAME container "
for i in $(seq 1 20); do
  if [ "$(sudo podman inspect -f "{{.State.Health.Status}}" $CON_NAME)" == "healthy" ]; then
    printf ' OK'
    echo -e "\n$(sudo podman logs $CON_NAME 2>/dev/null | grep 'password:') for your pi-hole: https://${CON_IP}/admin/"
    exit 0
  else
    sleep 3
    printf '.'
  fi

  if [ $i -eq 20 ]; then
    echo -e "\nTimed out waiting for Pi-hole start, consult your container logs for more info (\`docker logs pihole\`)"
    exit 1
  fi
done
